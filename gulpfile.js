'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');

gulp.task('sass', function () {
    return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'))
        .pipe(minifyCSS());
});

gulp.task('min', ['sass'], function () {
    return gulp.src('./css/*.css')
        .pipe(minifyCSS())
        .pipe(gulp.dest('./css'));
});

gulp.task('default', ['sass', 'min']);

gulp.task('watch', function () {
    gulp.watch('./css/*.scss', ['sass', 'min']);
});
