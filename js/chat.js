define([
    'config',
    'jquery',
    'underscore',
    'backbone',
    'socket.io',
    'js/chat/views/rooms',
    'js/chat/collections/rooms_collection',
    'js/chat/views/users',
    'js/chat/collections/users_collection',
    'js/chat/views/messages',
    'js/chat/collections/messages_collection',
    'js/messenger',
    'js/chat/views/login',
    'js/chat/views/createRoom',
    'js/chat/views/sendMessage'
], function(config, $, _, Backbone, io, roomsView, roomsCollection, usersView, usersCollection, messagesView, messagesCollection, messenger, loginView, createRoomView, sendMessageView) {
    var Chat = {
        username: '',
        relogin: false,
        currentRoomId: null,
        mediator: _.extend({}, Backbone.Events)
    };

    Chat.start = function() {
        var self = this;

        this.roomsCollection = new roomsCollection();
        this.roomsView = new roomsView({collection: this.roomsCollection, mediator: this.mediator});

        this.usersCollection = new usersCollection();
        this.usersView = new usersView({collection: this.usersCollection, mediator: this.mediator});

        this.messagesCollection = new messagesCollection();
        this.messagesView = new messagesView({collection: this.messagesCollection, mediator: this.mediator});

        var messengerEvents = {
            setup: function (data) {
                self.username = data.username || '';
                $('.loginForm').addClass('hidden');
                $('.chat').removeClass('hidden');
                $('#usernameLabel').text(self.username);
                console.log('setup', data);
                self.mediator.trigger('username:change', self.username);
                self.roomsCollection.reset(data.rooms || []);
            },
            connect: function (data) {
                console.log('connected');
                $('.splash').addClass('hidden');
                $('.chat_status').removeClass('offline');
                if(this.relogin){
                    self.messengerClient.login(self.username);
                    self.relogin = false;
                }
            },
            disconnect: function (data) {
                console.log('disconnected');
                $('.chat_status').addClass('offline');
                $('.splash').removeClass('hidden');
                this.relogin = true;
            },
            connect_timeout: function(data) {
                console.log('time out');
            },
            appError: function (data) {
                console.log('error', data);
                alert(data.message || 'error');
            },
            'room switched': function (data) {
                self.usersCollection.reset(data.users || []);
                self.messagesCollection.reset(data.messages || []);
                console.log('switch', data);
            },
            'room created': function (data) {
                self.roomsCollection.add(data);
                console.log('room created', data);
            },
            'message created': function (data) {
                console.log('message', data);
                self.messagesCollection.add(data);

                if ((data || data.user || data.user.name) && (data.user.name !== self.username)) {
                    self.notification(data);
                }
            },
            'room removed': function (data) {
                var room = self.roomsCollection.findWhere({_id: data._id});
                self.roomsCollection.remove(room);
            },
            'user joined': function(data) {
                console.log('joined', data);
                _.each(data, function(user){
                    self.usersCollection.add(user);
                });
            },
            'user left': function(data) {
                console.log('left', data);
                _.each(data, function(user){
                    var userModel = self.usersCollection.findWhere({_id: user._id});
                    self.usersCollection.remove(userModel);
                });
            },
            'room updated': function(data){
                var room = self.roomsCollection.findWhere({_id: data._id});
                room.set('name', data.name);
            }
        };

        this.messengerClient = new messenger({
            server: config.server || '',
            events: messengerEvents
        });

        this.mediator.listenTo(this.roomsView, 'room:click', this.messengerClient.switchRoom);
        this.mediator.listenTo(this.roomsView, 'room:delete', this.messengerClient.deleteRoom);
        this.mediator.listenTo(this.roomsView, 'room:rename', this.messengerClient.editRoom);

        this.loginForm = new loginView();
        this.loginForm.render();

        this.mediator.listenTo(this.loginForm, 'login:login', this.messengerClient.login);

        this.createRoomView = new createRoomView();

        this.mediator.listenTo(this.createRoomView, 'room:create', this.messengerClient.newRoom);

        this.sendMessageView = new sendMessageView();

        this.mediator.listenTo(this.sendMessageView, 'message:send', this.messengerClient.newMessage);

        $('input').keyup(function(e){
            if(e.keyCode == 13){
                $(this).trigger('enter');
            }
        });

        $('.left.glyphicon-align-justify').click(function() {
            if($('.right_panel').hasClass('col-xs-10')) {
                $('.right_panel').addClass('hidden-xs').removeClass('col-xs-10');
                $('.center_panel').removeClass('col-xs-2');
            }
            $('.left_panel').toggleClass('hidden-xs');
            $('.left_panel').toggleClass('col-xs-10');
            $('.center_panel').toggleClass('col-xs-2');
        });
        $('.right.glyphicon-align-justify').click(function() {
            if($('.left_panel').hasClass('col-xs-10')) {
                $('.left_panel').addClass('hidden-xs').removeClass('col-xs-10');
                $('.center_panel').removeClass('col-xs-2');
            }
            $('.right_panel').toggleClass('hidden-xs');
            $('.right_panel').toggleClass('col-xs-10');
            $('.center_panel').toggleClass('col-xs-2');
        });
    };

    Chat.notification = function (data) {
        var notification = new Notification(
            "New message from: " + data.user.name,
            {
                body: data.content || ''
            }
        );
        $('.audio').html('<audio autoplay="autoplay" style="display:none;"><source src="alert.mp3" /></audio>');
    };

    return Chat;
});
