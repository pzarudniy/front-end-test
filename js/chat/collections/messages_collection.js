define([
    'jquery',
    'underscore',
    'backbone',
    'js/chat/models/message'
], function($, _, Backbone, messageModel) {
    var collection = Backbone.Collection.extend({
        model: messageModel,
        initialize: function () {
        }
    });

    return collection;
});