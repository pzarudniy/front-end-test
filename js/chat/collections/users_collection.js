define([
    'jquery',
    'underscore',
    'backbone',
    'js/chat/models/user'
], function($, _, Backbone, userModel) {
    var collection = Backbone.Collection.extend({
        model: userModel,
        initialize: function () {
        }
    });

    return collection;
});