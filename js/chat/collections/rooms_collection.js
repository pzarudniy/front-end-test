define([
    'jquery',
    'underscore',
    'backbone',
    'js/chat/models/room'
], function($, _, Backbone, roomModel) {
    var collection = Backbone.Collection.extend({
        model: roomModel,
        initialize: function (options) {
        }
    });

    return collection;
});