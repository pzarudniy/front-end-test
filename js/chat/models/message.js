define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var model = Backbone.Model.extend({
        defaults: {
            content: '',
            user: '',
            my: false,
            time: ''
        }
    });

    return model;
});
