define([
    'jquery',
    'underscore',
    'backbone',
    'bower_components/text/text!js/chat/templates/login.html'
], function($, _, Backbone, loginTemplate) {
    var view = Backbone.View.extend({
        el: '.loginForm',
        initialize: function(){
            this.template = _.template(loginTemplate);
        },
        events:{
            'click .loginBtn': 'login'
        },
        login: function(e){
            e.preventDefault();
            var username = this.$el.find('#username').val();
            this.trigger('login:login', username);

        },
        render: function(){
            this.$el.html(this.template());
            return this;
        }
    });

    return view;
});