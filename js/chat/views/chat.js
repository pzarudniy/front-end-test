define([
    'jquery',
    'underscore',
    'backbone',
    'bower_components/text/text!js/chat/templates/messages.html'
], function($, _, Backbone, messagesTemplate) {
    var view = Backbone.View.extend({
        el: 'div.messages ul',
        initialize: function(){
            this.template = _.template(messagesTemplate);
        },
        events:{

        },
        render: function(){
            this.$el.html(this.template());
            return this;
        }
    });

    return view;
});

