define([
    'jquery',
    'underscore',
    'backbone',
    'bower_components/text/text!js/chat/templates/messages.html'
], function($, _, Backbone, messagesTemplate) {
    var view = Backbone.View.extend({
        username: '',
        el: 'div.messages ul',
        initialize: function(opts){
            _.template.formatdate = function (stamp) {
                var d = new Date(stamp),
                    fragments = [
                        d.getHours(),
                        d.getMinutes()
                    ];
                return fragments.join(':');
            };
            this.template = _.template(messagesTemplate);
            this.collection.on('reset add remove', this.render, this);
            this.listenTo(opts.mediator, 'username:change', this.newUsername);
        },
        events:{

        },
        newUsername: function(name){
            this.username = name;
            this.render();
        },
        render: function(){
            this.$el.html(this.template({ messages : this.collection.toJSON(), username: this.username }));
            return this;
        }
    });

    return view;
});

