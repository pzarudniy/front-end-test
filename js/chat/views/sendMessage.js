define([
    'jquery',
    'underscore',
    'backbone',
    'bower_components/text/text!js/chat/templates/send_message.html'
], function($, _, Backbone, sendMessageTemplate) {
    var view = Backbone.View.extend({
        el: 'div.send_message',
        initialize: function(){
            this.template = _.template(sendMessageTemplate);
            this.render();
        },
        events:{
            'click #endMessageBtn': 'sendMessage',
            'enter #message': 'sendMessage'

        },
        sendMessage: function(e){
            var messageEl = this.$el.find('#message'),
                message = messageEl.val();
            if (message) {
                this.trigger('message:send', message);
                messageEl.val('');
            }
        },
        render: function(){
            this.$el.html(this.template({}));
            return this;
        }
    });

    return view;
});
