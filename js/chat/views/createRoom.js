define([
    'jquery',
    'underscore',
    'backbone',
    'bower_components/text/text!js/chat/templates/create_room.html'
], function($, _, Backbone, createRoomTemplate) {
    var view = Backbone.View.extend({
        el: 'div.create_room',
        initialize: function(){
            this.template = _.template(createRoomTemplate);
            this.render();
        },
        events:{
            'click #createRoomBtn': 'createRoom',
            'enter #newRoomName': 'createRoom'

        },
        createRoom: function(e){
            var new_room = this.$el.find('#newRoomName');
            e.preventDefault();
            if (new_room.val()) {
                this.trigger('room:create', new_room.val());
                new_room.val('');
            }
        },
        render: function(){
            this.$el.html(this.template({}));
            return this;
        }
    });

    return view;
});
