define([
    'jquery',
    'underscore',
    'backbone',
    'bower_components/text/text!js/chat/templates/users.html'
], function($, _, Backbone, usersTemplate) {
    var view = Backbone.View.extend({
        username: '',
        el: 'div.users ul',
        initialize: function(opts){
            this.template = _.template(usersTemplate);
            this.collection.on('reset add remove', this.render, this);
            this.listenTo(opts.mediator, 'username:change', this.newUsername);
        },
        newUsername: function(name){
            this.username = name;
            this.render();
        },
        events:{

        },
        render: function(){
            this.$el.html(this.template({ users : this.collection.toJSON(), username: this.username }));
            return this;
        }
    });

    return view;
});
