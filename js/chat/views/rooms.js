define([
    'jquery',
    'underscore',
    'backbone',
    'bower_components/text/text!js/chat/templates/rooms.html'
], function($, _, Backbone, roomsTemplate) {
    var view = Backbone.View.extend({
        username: '',
        el: 'div.rooms ul',
        initialize: function(opts){
            this.template = _.template(roomsTemplate);
            this.collection.on('reset add remove update change', this.render, this);
            this.listenTo(opts.mediator, 'username:change', this.newUsername);
        },
        events:{
            'click li': 'roomClick',
            'enter input': 'editRoom',
            'blur input': 'editRoom'
        },
        newUsername: function(name){
            this.username = name;
            this.render();
        },
        roomClick: function(e){
            e.stopPropagation();
            var target = $(e.target),
                roomId,
                name;
            if(target.hasClass('glyphicon')){
                roomId = target.parent().parent().data('id');
                name = target.parent().parent().data('name');
                if(target.hasClass('glyphicon-remove')){
                    this.trigger('room:delete', roomId);
                }
                if(target.hasClass('glyphicon-edit')){
                    target.parent().siblings('label').addClass('hidden').siblings('input').removeClass('hidden');
                }
            }else{
                roomId = $(e.target).parent().data('id');
                if(roomId && roomId !== self.currentRoomId){
                    this.trigger('room:click', roomId);
                }

            }
        },
        editRoom: function(e){
            var target = $(e.target),
                roomId;
            if(target.val() !== target.data('name')){
                roomId = target.parent().data('id');
                this.trigger('room:rename', roomId, target.val());
            }
            target.addClass('hidden').siblings('label').removeClass('hidden');
        },
        render: function(){
            this.$el.html(this.template({ rooms : this.collection.toJSON(), username: this.username }));
            return this;
        }
    });

    return view;
});
