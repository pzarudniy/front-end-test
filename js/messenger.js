define([
    'socket.io'
], function(io){
    var init = function(opts){
        var messenger = {},
            socket = io(opts.server || '');

        _.each(opts.events || [], function(callback, event){
            socket.on(event, _.isFunction(callback) ? callback : _.noop);
        });

        messenger.login = function(username){
            socket.emit('login', { username: username});
        };

        messenger.newRoom = function(roomName){
            socket.emit('new room', {name: roomName});
        };

        messenger.switchRoom = function(roomId){
            socket.emit('switch room', {_id: roomId});
        };

        messenger.editRoom = function(roomId, roomName){
            socket.emit('edit room', {id: roomId, name: roomName, remove: false});
        };

        messenger.deleteRoom = function(roomId, roomName){
            socket.emit('edit room', {id: roomId, name: roomName, remove: true});
        };

        messenger.newMessage = function(message){
            socket.emit('new message', {message: message});
        };

        return messenger;
    };

    return init;

});
